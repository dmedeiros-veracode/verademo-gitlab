# Verademo Gitlab Example

## Description

This application demonstrates using Veracode SAST Analysis Tools within the SDLC process to properly manage and monitor development risk.

The .gitlab-ci.yaml file is segmented into separate jobs to perform then build, security analysis, testing and deployment as needed for the application context. This simple example shows how to configure the event of a scheduled trigger vs that of a push being perform within the pipeline.

'testing